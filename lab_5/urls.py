from django.urls import path
from .views import *

app_name = 'lab_5'

urlpatterns = [
    path('', index, name='index'),
    path('project', project, name='project'),
    path('contact', contact, name='contact'),
    path('form', activity_form, name='activity_form'),
    path('form/schedules', all_schedule, name='all_schedule'),
    path('form/delete_all', delete_all, name="delete_all"),
]
