from django.conf.urls import url
from .views import *

app_name = 'lab_4'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^project$', project, name='project'),
    url(r'^contact$', contact, name='contact'),
    url(r'^register$', register, name='register'),
]
